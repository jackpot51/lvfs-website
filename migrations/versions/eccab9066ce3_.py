# Custom template
"""empty message

Revision ID: eccab9066ce3
Revises: 6b9c338048c9
Create Date: 2023-05-16 09:56:22.030908

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "eccab9066ce3"
down_revision = "6b9c338048c9"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("protocol", schema=None) as batch_op:
        batch_op.add_column(sa.Column("signing_algorithm", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("comment", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("protocol", schema=None) as batch_op:
        batch_op.drop_column("comment")
        batch_op.drop_column("signing_algorithm")
