"""empty message

Revision ID: b9aaf7bd9cac
Revises: 587aebb3c40f
Create Date: 2023-06-05 16:14:29.064877

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "b9aaf7bd9cac"
down_revision = "587aebb3c40f"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("component_shard_infos", schema=None) as batch_op:
        batch_op.add_column(sa.Column("name", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("component_shard_infos", schema=None) as batch_op:
        batch_op.drop_column("name")
