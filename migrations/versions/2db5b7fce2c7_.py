"""empty message

Revision ID: 2db5b7fce2c7
Revises: b9aaf7bd9cac
Create Date: 2023-06-13 14:38:06.358579

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "2db5b7fce2c7"
down_revision = "b9aaf7bd9cac"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("tasks", schema=None) as batch_op:
        batch_op.add_column(sa.Column("force", sa.Boolean(), nullable=True))


def downgrade():
    with op.batch_alter_table("tasks", schema=None) as batch_op:
        batch_op.drop_column("force")
