#!/usr/bin/python3
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=consider-using-dict-items


from lvfs import db

from lvfs.pluginloader import PluginBase, PluginSettingBool
from lvfs.components.models import Component, ComponentGuid, ComponentRequirement
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote
from lvfs.tests.models import Test


def _test_dropped_guid(test: Test, md: Component) -> None:
    old_guids: dict[str, Firmware] = {}
    for guid in (
        db.session.query(ComponentGuid)
        .distinct(ComponentGuid.value)
        .join(Component)
        .filter(Component.appstream_id == md.appstream_id)
        .join(Firmware)
        .filter(Firmware.firmware_id < md.fw.firmware_id)
        .join(Remote)
        .filter(Remote.name.notin_(["deleted", "private"]))
    ):
        old_guids[guid.value] = guid.md.fw

    for old_guid in old_guids:
        if old_guid not in md.guid_values:
            test.add_fail(
                "GUID dropped",
                f"Component {md.appstream_id} drops GUID {old_guid} previously supported "
                f"in firmware {old_guids[old_guid].firmware_id!s} "
                "-- perhaps the AppStream ID is not unique?",
            )


def _test_dropped_chid(test: Test, md: Component) -> None:
    old_guids: dict[str, Firmware] = {}
    for req in (
        db.session.query(ComponentRequirement)
        .filter(ComponentRequirement.kind == "hardware")
        .distinct(ComponentRequirement.value)
        .join(Component)
        .filter(Component.appstream_id == md.appstream_id)
        .join(Firmware)
        .filter(Firmware.firmware_id < md.fw.firmware_id)
        .join(Remote)
        .filter(Remote.name != "deleted")
    ):
        old_guids[req.value] = req.md.fw

    md_chid_values: list[str] = []
    for req in md.requirements:
        if req.kind == "hardware" and req.value not in md_chid_values:
            md_chid_values.append(req.value)

    for old_guid in old_guids:
        if old_guid not in md_chid_values:
            test.add_fail(
                "CHID dropped",
                f"Component {md.appstream_id} drops Computer Hardware ID {old_guid} previously added "
                f"in firmware {old_guids[old_guid].firmware_id!s} -- "
                "perhaps the AppStream ID is not unique?",
            )


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self)
        self.name = "GUIDs"
        self.summary = "Check firmware GUIDs are correct"
        self.settings.append(
            PluginSettingBool(key="guids_enable", name="Enabled", default=True)
        )

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:
        # check if the file dropped a GUID previously supported
        _test_dropped_guid(test, md)
        _test_dropped_chid(test, md)
