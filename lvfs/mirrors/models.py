#!/usr/bin/python3
#
# Copyright (C) 2023 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import datetime
import os
from typing import Optional

from flask import current_app as app

from sqlalchemy import Column, Integer, BigInteger, Text, Boolean, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from lvfs import db


class MirrorItem(db.Model):  # type: ignore
    __tablename__ = "mirror_checksums"
    mirror_checksum_id = Column(Integer, primary_key=True)
    mirror_id = Column(
        Integer, ForeignKey("mirrors.mirror_id"), nullable=False, index=True
    )
    valid = Column(Boolean, default=False)
    basename = Column(Text, nullable=False, index=True)
    checksum = Column(Text, nullable=False, index=True)
    size = Column(Integer, nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    mirror = relationship("Mirror")

    @property
    def absolute_path(self) -> str:
        return os.path.join(app.config["MIRROR_DIR"], self.mirror.name, self.basename)

    @property
    def blob(self) -> bytes:
        with open(self.absolute_path, "rb") as f:
            return f.read()

    def __repr__(self) -> str:
        return f"MirrorItem({self.mirror_checksum_id}:{self.basename}:{self.checksum}:{self.size})"


class MirrorFilter(db.Model):  # type: ignore
    __tablename__ = "mirror_filters"
    mirror_tag_id = Column(Integer, primary_key=True)
    mirror_id = Column(
        Integer, ForeignKey("mirrors.mirror_id"), nullable=False, index=True
    )
    key = Column(Text, nullable=False)
    value = Column(Text, nullable=False)

    mirror = relationship("Mirror")

    def __repr__(self) -> str:
        return f"MirrorFilter({self.mirror_tag_id}:{self.key}={self.value})"


class Mirror(db.Model):  # type: ignore
    __tablename__ = "mirrors"
    mirror_id = Column(Integer, primary_key=True)
    remote_id = Column(Integer, ForeignKey("remotes.remote_id"), default=None)
    task_id = Column(Integer, ForeignKey("tasks.task_id"), default=None)
    size_now = Column(BigInteger, default=None)
    size_total = Column(BigInteger, default=None)
    name = Column(Text, nullable=False, index=True, unique=True)
    enabled = Column(Boolean, default=False)
    remove_deleted = Column(Boolean, default=False)
    uri = Column(Text, default=None, unique=True)
    auth_username = Column(Text, default=None)
    auth_token = Column(Text, default=None)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    metadata_basename = Column(Text, default=None)
    comment = Column(Text, default=None)

    filters = relationship(
        "MirrorFilter", back_populates="mirror", cascade="all,delete,delete-orphan"
    )
    items = relationship(
        "MirrorItem", back_populates="mirror", cascade="all,delete,delete-orphan"
    )
    user = relationship("User")
    remote = relationship("Remote")
    task = relationship("Task")

    @property
    def metadata_blob(self) -> Optional[bytes]:
        if not self.metadata_basename:
            return None
        with open(
            os.path.join(app.config["MIRROR_DIR"], self.name, self.metadata_basename),
            "rb",
        ) as f:
            return f.read()

    @property
    def color(self) -> str:
        if self.task:
            return self.task.color
        return "danger"

    def __repr__(self) -> str:
        return f"Mirror({self.mirror_id}:{self.name})"
